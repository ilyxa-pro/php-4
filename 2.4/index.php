<?php
error_reporting(E_ALL);
header('Content-Type: text/html; charset=utf-8');
session_start();

if (isset($_COOKIE['name_guest']) || isset($_SESSION['name'])){
  header('Location: list.php');
}
if (isset($_POST['guest']) && $_POST['guest'] != "" ) { //срабатывает со 2 раза
  setcookie('name_guest', $_POST['guest'], time()+36000);
  header('Location: list.php');
  exit;
}
if (isset($_POST['user']) && $_POST["user"] != "") {
  $json = file_get_contents('login.json');
  $loginList = json_decode($json,true);
  foreach ($loginList as $login => $value) {
    if ( ($_POST['user'] == $value["user"]) && ($_POST['password'] == $value["password"])) {
      session_name($_POST['user']);
      $_SESSION["name"] = $value["user"];
      $_SESSION["rights"] = 1;
      header('Location: list.php');
      exit;
    }
  }
}

$noAuthParams = !(isset($_COOKIE['name_guest']) || isset($_SESSION['name']));
if ($noAuthParams) {
header('HTTP/1.1 401');
?>
<h2>Авторизуйтесь</h2>
<form action="index.php" method="post">
<input type="txt" name="user">
<input type="password" name="password">
<h2>Войти как гость</h2>
<input type="txt" name="guest">
<input type="submit">
</form>
<?php
}?>
