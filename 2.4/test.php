<?php
error_reporting(E_ALL);
session_start();
$noAuthParams = isset($_COOKIE['name_guest']) || isset($_SESSION['name']);
if ($noAuthParams) {
isset($_COOKIE['name_guest']) ? $name=$_COOKIE['name_guest'] : $name=$_SESSION['name'];
header('Content-Type: text/html; charset=utf-8');
$op = 0;
if (!isset($_GET['id'])) {
  header('Location: ../list.php'); //Если тест не выбран возврат в список тестов
} else {
$getid = $_GET['id'];
if(!$json = file_get_contents(__DIR__."/test/".$getid.".json")) {
  header("HTTP/1.0 404 Not Found");
  exit;
}
$lose = 0;
$test = json_decode($json,true);
if ($test === null) {
  echo "Извените файл теста json некорректен.";
}
else { ?>
<p><em>Внимание сертификаты выдаются только при прохождении теста более чем на 80%</em></p>
<form action="./test.php" method="get">
<input hidden="hidden" name="id" value="<?php echo $_GET['id']?>">
<?php  foreach ($test as $tests => $value):?>
<p>Вопрос номер <em><?php $value['n']?></em></p>
<label name="<?php  echo $value['n']?>"><?php echo $value['Question']?>
<input type="text" value="0" name="Question[<?php  echo $value['n']?>]"  placeholder="<?php  echo $value['Question'] ?>"></label>
<?php $op++;?>
<?php  endforeach  ?>
<input type="submit">
</form>
<?php }
if (isset($_GET["Question"])) {
 $max = count($test);
 foreach ($test as $tests => $value) {
   $num = $value['n'];
   if ($value['Answer'] == $_GET["Question"][$num] ) {
     echo "Ваш ответ на <strong>вопрос ". $value['n'] ."</strong> правильный =)<br>";
   }
   else {
     $lose++;
     echo "Ваш ответ на <strong>вопрос " . $value['n'] . "</strong> неправильный =(<br>";
   }
 }
 $win = (1.0-$lose/$max) * 100;
if ($win > 80) {
  echo "Поздравляем вы прошли тест. Процент правильных ответов $win%";
  $_SESSION['win'] = $win;
?>
<form action="./serf.php" method="post">
<button type="submit" name="button">Получить сертификат</button>
</form>
<?php
}
else {
 echo "К сожалению вы провалили тест. Процент правильных ответов $win%";
}
}
}
} else { header('HTTP/1.1 403 Forbidden');?>
Нету прав для просмотра странице перейти на стараницу <a href="./">авторизации</a>
<?php }?>
