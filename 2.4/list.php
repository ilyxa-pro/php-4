<?php  error_reporting(E_ALL);
session_start();
header('Content-Type: text/html; charset=utf-8');
$noAuthParams = isset($_COOKIE['name_guest']) || isset($_SESSION['name']);
if ($noAuthParams) {
isset($_COOKIE['name_guest']) ? $name=$_COOKIE['name_guest'] : $name=$_SESSION['name'];
$dir    = __DIR__ . '/test';
$tests = scandir($dir);
unset($tests[1]); // удаление ..
unset($tests[0]); // удаление .
sort($tests, SORT_NUMERIC); //  SORT_NUMERIC сравнение элементов как числа
$max = count($tests);
function number($item) {
  $num = preg_split('/\./',$item);
  return $num[0];
}
?>
<div class="">
  <form class="" action="exit.php" method="post">
    <p>Вы зашли как <?php echo $name?></p>
    <button>Выйти</button>
  </form>
</div>

<?php for ($i=0; $i < $max; $i++) {?>
<div><a href="test.php?id=<?php echo number($tests[$i])?>">Пройти <?php echo number($tests[$i])?> Тест</a></div>
<?php if (isset($_SESSION['name'])) {?>
<div><a href="delete.php?delete=<?php echo $tests[$i]?>">Удалить тест</a></div>
<?php }
}
if (isset($_SESSION['name'])) {?>
<div><a href="admin.php">Добавить тест</a></div>
<div><a href="delete.php">Удалить тест</a></div>
<?php }
} else
{
header('HTTP/1.1 403 Forbidden');
?>
Нету прав для просмотра странице перейти на стараницу <a href="./">авторизации</a>
<?php
}
?>
