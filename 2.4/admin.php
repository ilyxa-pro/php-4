<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
$noAuthParams = isset($_SESSION['name']);
if ($noAuthParams) {
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Занятие 2.2 «Обработка форм»</title>
</head>
<body>
<div>
<h1>Занятие 2.2 «Обработка форм»</h1>
<form action="about.php" method="post" enctype="multipart/form-data">
  <label>Зарузите файл теста
  <input type="file" name="filetest"></label>
  <input type="submit" value="Отправить">
</form>
</div>
</body>
</html>
<?php} else {
header('HTTP/1.1 403 Forbidden');
}
?>
