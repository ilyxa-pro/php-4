<?php
error_reporting(E_ALL);
header('Content-Type: text/html; charset=utf-8');
$dir = __DIR__;
$pathTest = __DIR__ . "/test/";
if (!empty($_FILES['filetest'])) { // Проверка расширения json
  $fileName = $_FILES['filetest']['name'];
  $ext = substr($fileName, strrpos($fileName, '.') + 1);
  $fileType = array("json","JSON");
  if (in_array($ext, $fileType)) {
    $dir    = __DIR__ . '/test';
    $tests = scandir($dir);
    unset($tests[1]); // удаление ..
    unset($tests[0]); // удаление .
    sort($tests);
    natsort($tests);// Сортировка элементов от меньшего к большему
    $newTest = array_pop($tests) + 1;
    if (move_uploaded_file($_FILES['filetest']['tmp_name'], $pathTest . $newTest . '.json')) {
    header("Location: list.php");
  }
  }
  else {
    echo "Неверное расширение файла. Файл теста должен быть в формате JSON";
    }
  }
else {
  echo "Файл не загружен";
}
