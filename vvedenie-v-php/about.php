<?php
$userName = 'Илья';
$userAge = '24';
$userMail = 'ilyxaang@gmail.com';
$userCity = 'Красноярск, Иркустк';
$userAboutMe = 'Начинающий фронтэнд разбработчик отчаино пытающийся стать FullStack';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>1.1 Введиние в PHP</title>
</head>
<body>
<div>

<?php if ($userName) { ?>
<div>
 <h2>Информация о пользователе <?= $userName ?></h2>
	<p>Имя: <strong><?= $userName ?></strong></p>
 <p>Возраст: <strong><?= $userAge ?></strong></p>
	<p>Адрес электронной почты: <strong><a href="mailto:<?= $userMail ?>"><?= $userMail ?></a></strong></p>
	<p>Город: <strong><?= $userCity ?></strong></p>
	<p>О себе: <strong><?= $userAboutMe ?></strong></p>
</div>
<?php } else { ?>
<div><h2>Пользователь не найден</h2></div>
<?php }?>
</div>
</body>
</html>
