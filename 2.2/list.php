<?php error_reporting(E_ALL);
header('Content-Type: text/html; charset=utf-8');
$dir    = __DIR__ . '/test';
$tests = scandir($dir);
unset($tests[1]); // удаление ..
unset($tests[0]); // удаление .
sort($tests, SORT_NUMERIC); //  SORT_NUMERIC сравнение элементов как числа
$max = count($tests);
function number($item) {
  $num = preg_split('/\./',$item);
  return $num[0];
}
for ($i=0; $i < $max; $i++):?>
<div><a href="test.php?id=<?php echo number($tests[$i])?>">Пройти <?php echo number($tests[$i])?> Тест</a></div>
<?php endfor ?>
