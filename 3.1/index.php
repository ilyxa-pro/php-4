<?php  error_reporting(E_ALL);
session_start();
header('Content-Type: text/html; charset=utf-8');
class Sweet
{
  public $name;
  protected $price;
  public $ingredients;
  public $timeLife;
  public $creation;
  public function __construct($name,$price,$ingredients,$timeLife,$creation) {
    $this->name = $name;
    $this->price = $price;
    $this->ingredients = $ingredients;
    $this->timeLife = $timeLife;
    $this->creation = date_create($creation);
  }
  public function priceShow() { // показать цену.
    echo $this->price;
  }
  public function getPrice($price)
  {
    $this->price = $price;
  }
  public function sale($value)
  {
    if (0 <= $value and $value <= 100) {
      $this->price = $this->price - (($this->price*$value)*0.01);
      return $this->price;
    } else echo "<br><span style='color:red'>Необходимо задать величину скидки в процентах</span>";
  }
  public function lifeDay() // Определяем сколько осталось дней до истечения срока годности
  {
    $dieDay =  date_add($this->creation, date_interval_create_from_date_string( $this->timeLife .' days'));
    $dieDay = round(date_format(new DateTime($dieDay->format('y-m-d')), 'U')/86400); // день когда товар просрочится
    // атрибут u - 	Количество секунд, прошедших с начала Эпохи Unix (The Unix Epoch, 1 января 1970, 00:00:00 GMT)
    $today = round(date_format(new DateTime(date("y-m-d")), 'U')/86400); // сегодняшний день
    $residue = $dieDay - $today;
    if ($residue <= 0) {
      echo "<br>товар просрочен";
      return $residue;
    } else {
    echo "<br>nродукт " . $this->name . " испортится через  $residue дней";
    return $residue;
    }

  }
  public function saleInTimeLife($value,$day) // Скидка за несколько дней до просрочки
  {
    $residue = $this->lifeDay();//Узнаем сколько осталось им до потери срока годности
    if ($residue <= $day and $residue > 0) {
      $this->sale($value); // делаем скидку
    }
  }
}
class Candies extends Sweet
{
    public $mark;
}
//__construct($name,$price,$ingredients,$timeLife,$creation)
$PtichieSlast = new Candies('«Птичья сласть» классическая', 125 ,'шоколад, птичье молоко', 40 ,'2016-10-23');
$AlpenGoldBlack = new Candies('Альпен голд черный шоколад', 55 ,'темный шоколад', 500 ,'2015-07-23');
$AlpenGoldWhite = new Candies('Альпен голд белый шоколад', 70 ,' белый шоколад', 50 ,'2016-10-23');
$AlpenGoldMilk = new Candies('Альпен голд молочный', 100 ,'молочный шоколад', 50 ,'2016-9-23');
$Rafaelo = new Candies('Рафаэло', 150 ,'шоколад, птичье молоко', 60 ,'2016-8-23');

echo'<br> Товар ' . $PtichieSlast->name .' Стоимостью '. $PtichieSlast->priceShow();
echo '<br> Делаем скидку на 50 процентов если осталось менее 20 дней до истечения срока годности' .$PtichieSlast->saleInTimeLife(50,20);
echo '<br>'.$PtichieSlast->priceShow();
?>
